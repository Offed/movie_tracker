import 'package:flutter/material.dart';
import 'package:movietracker/services/MovieSuggester.dart';
import 'package:movietracker/services/auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:movietracker/models/movie.dart';
import 'dart:async';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Movie> _movieList;
  List<Movie> _visibleList;

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final ratingController = TextEditingController();
  final searchController = TextEditingController();
  final TextEditingController movienameController = TextEditingController();

  String _selectedMovie;

  Movie modifyingMovie;

  StreamSubscription<Event> _onMovieAddedSubscription;
  StreamSubscription<Event> _onMovieChangedSubscription;

  Query _movieQuery;

  @override
  void initState() {
    super.initState();

    //_checkEmailVerification();

    _movieList = new List();
    _visibleList = new List();
    _movieQuery = _database
        .reference()
        .child("movie")
        .orderByChild("userId")
        .equalTo(widget.userId);
    _onMovieAddedSubscription = _movieQuery.onChildAdded.listen(onEntryAdded);
    _onMovieChangedSubscription =
        _movieQuery.onChildChanged.listen(onEntryChanged);
  }

  querysetChange(String text) async {
    _visibleList = List.from(_movieList);
    if(text == ""){
      setState(() {

      });
      return;
    };
    setState(() {
      _visibleList.removeWhere((element) => !element.moviename.toLowerCase().contains(text.toLowerCase()));
    });
  }

  @override
  void dispose() {
    _onMovieAddedSubscription.cancel();
    _onMovieChangedSubscription.cancel();
    super.dispose();
  }

  onEntryChanged(Event event) {
    var oldEntry = _movieList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _movieList[_movieList.indexOf(oldEntry)] =
          Movie.fromSnapshot(event.snapshot);
      querysetChange(searchController.text.toString());
    });
  }

  onEntryAdded(Event event) {
    setState(() {
      _movieList.add(Movie.fromSnapshot(event.snapshot));
      querysetChange(searchController.text.toString());
    });
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  addNewMovie(String movieItem, num movierating) {
    if (movieItem.length > 0) {
      Movie movie = new Movie(movieItem.toString(), widget.userId, false);
      movie.movierating = movierating;
      _database.reference().child("movie").push().set(movie.toJson());
    }
    querysetChange(searchController.text.toString());
  }

  completeMovie(Movie movie) {
    movie.completed = !movie.completed;
    if (movie != null) {
      _database.reference().child("movie").child(movie.key).set(movie.toJson());
    }
    //querysetChange(searchController.text.toString());
  }

  modifyMovie(Movie movie, String movieItem, num movierating) {
    movie.moviename = movieItem;
    movie.movierating = movierating;
    if (movie != null) {
      _database.reference().child("movie").child(movie.key).set(movie.toJson());
    }
  }

  deleteMovie(String movieId, int index) {
    _database.reference().child("movie").child(movieId).remove().then((_) {
      print("Delete $movieId successful");
      _movieList.removeWhere((element) => element.key == movieId);
      _visibleList.removeWhere((element) => element.key == movieId);
      setState(() {

      });
    });
    querysetChange(searchController.text.toString());
    //searchController.clear();
  }



  showAddMovieDialog(BuildContext context) async {
    ratingController.clear();
    movienameController.clear();
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Column(
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Expanded(
                        child: TypeAheadFormField(
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: this.movienameController,
                          decoration:
                              InputDecoration(labelText: 'Choose movie')),
                      suggestionsCallback: (pattern) {
                        return getMovieSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          title: Text(suggestion.original_title),
                        );
                      },
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        this.movienameController.text =
                            suggestion.original_title;
                        this.ratingController.text =
                            suggestion.vote_average.toString();
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please select a movie';
                        }
                      },
                      onSaved: (value) => this._selectedMovie = value,
                    ))
                  ],
                ),
                new Expanded(
                    child: new TextField(
                  controller: ratingController,
                  decoration: new InputDecoration(
                    labelText: 'Add rating',
                  ),
                ))
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Save'),
                  onPressed: () {
                    addNewMovie(movienameController.text.toString(),
                        num.parse(ratingController.text.toString()));
                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  showModifyMovieDialog(BuildContext context) async {
    ratingController.text = modifyingMovie.movierating.toString();
    movienameController.text = modifyingMovie.moviename;
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Column(
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Expanded(
                        child: TypeAheadFormField(
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: this.movienameController,
                          decoration:
                              InputDecoration(labelText: 'Choose movie')),
                      suggestionsCallback: (pattern) {
                        return getMovieSuggestions(pattern);
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          title: Text(suggestion.original_title),
                        );
                      },
                      transitionBuilder: (context, suggestionsBox, controller) {
                        return suggestionsBox;
                      },
                      onSuggestionSelected: (suggestion) {
                        this.movienameController.text =
                            suggestion.original_title;
                        this.ratingController.text =
                            suggestion.vote_average.toString();
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please select a movie';
                        }
                      },
                      onSaved: (value) => this._selectedMovie = value,
                    ))
                  ],
                ),
                new Expanded(
                    child: new TextField(
                  controller: ratingController,
                  decoration: new InputDecoration(
                    labelText: 'Add rating',
                  ),
                ))
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Save'),
                  onPressed: () {
                    modifyMovie(
                        modifyingMovie,
                        movienameController.text.toString(),
                        num.parse(ratingController.text.toString()));
                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  Widget showMovieList() {
    if (_visibleList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: _visibleList.length,
          itemBuilder: (BuildContext context, int index) {
            String movieId = _visibleList[index].key;
            String moviename = _visibleList[index].moviename;
            num movierating = _visibleList[index].movierating;
            bool completed = _visibleList[index].completed;
            String userId = _visibleList[index].userId;
            return Dismissible(
              key: Key(movieId),
              background: Container(color: Colors.red),
              onDismissed: (direction) async {
                deleteMovie(movieId, index);
                querysetChange(searchController.text.toString());
              },
              child: ListTile(
                onTap: () {
                  modifyingMovie = _visibleList[index];
                  showModifyMovieDialog(context);
                },
                title: Text(
                  moviename + ", " + movierating.toString(),
                  style: TextStyle(fontSize: 20.0),
                ),
                trailing: IconButton(
                    icon: (completed)
                        ? Icon(
                            Icons.done_outline,
                            color: Colors.green,
                            size: 20.0,
                          )
                        : Icon(Icons.done, color: Colors.grey, size: 20.0),
                    onPressed: () {
                      completeMovie(_visibleList[index]);
                    }),
              ),
            );
          });
    } else {
      return Center(
          child: Text(
        "Welcome. Your list is empty",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30.0),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: new AppBar(
          title: new Text('Movie Tracker'),
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.black)),
                onPressed: signOut)
          ],
        ),
        body: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Expanded(
                    child: new TextField(
                  controller: searchController,
                  onChanged: querysetChange,
                  decoration: new InputDecoration(
                    labelText: 'Search',
                  ),
                ))
              ],
            ),
            new Row(
              children: <Widget>[new Expanded(child: showMovieList())],
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showAddMovieDialog(context);
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ));
  }
}
