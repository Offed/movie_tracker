import 'package:flutter/material.dart';
import 'package:movietracker/services/auth.dart';
import 'package:movietracker/pages/root_page.dart';

void main() {
  ErrorWidget.builder = (FlutterErrorDetails details) => Container();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Movie Tracker',
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: new RootPage(auth: new Auth()));
  }
}
