import 'package:firebase_database/firebase_database.dart';

class Movie {
  String key;
  String moviename;
  num movierating;
  bool completed;
  String userId;

  Movie(this.moviename, this.userId, this.completed);

  Movie.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        userId = snapshot.value["userId"],
        moviename = snapshot.value["moviename"],
        movierating = snapshot.value["movierating"],
        completed = snapshot.value["completed"];

  toJson() {
    return {
      "userId": userId,
      "moviename": moviename,
      "movierating": movierating,
      "completed": completed,
    };
  }
}
