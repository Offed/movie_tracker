

part 'jsonMovie.g.dart';


class JsonMovie {
  final String original_title;
  final num vote_average;

  JsonMovie({this.original_title, this.vote_average});

  factory JsonMovie.fromJson(Map<String, dynamic> json) =>
      _$JsonMovieFromJson(json);

  Map<String, dynamic> toJson() => _$JsonMovieToJson(this);
}


class JsonMovieResponse {
  final List<JsonMovie> results;

  JsonMovieResponse({this.results});

  factory JsonMovieResponse.fromJson(Map<String, dynamic> json) =>
      _$JsonMovieResponseFromJson(json);

  Map<String, dynamic> toJson() => _$JsonMovieResponseToJson(this);
}
