// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'jsonMovie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonMovie _$JsonMovieFromJson(Map<String, dynamic> json) {
  return JsonMovie(
    original_title: json['original_title'] as String,
    vote_average: json['vote_average'] as num,
  );
}

Map<String, dynamic> _$JsonMovieToJson(JsonMovie instance) => <String, dynamic>{
      'original_title': instance.original_title,
      'vote_average': instance.vote_average,
    };

JsonMovieResponse _$JsonMovieResponseFromJson(Map<String, dynamic> json) {
  return JsonMovieResponse(
    results: (json['results'] as List)
        ?.map((e) =>
            e == null ? null : JsonMovie.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$JsonMovieResponseToJson(JsonMovieResponse instance) =>
    <String, dynamic>{
      'results': instance.results,
    };
