import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movietracker/models/jsonMovie.dart';




Future<List<JsonMovie>> getMovieSuggestions(pattern) async {
  List<JsonMovie> list = new List<JsonMovie>();
  if(pattern == "") return list;

  final response = await http.get('https://api.themoviedb.org/3/search/movie\?api_key\=b67cf5e5937d794800a48aa688d0be9f\&language\=en-US\&query\=$pattern\&page\=1\&include_adult\=false');

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    JsonMovieResponse jsonMovieResponse = JsonMovieResponse.fromJson(jsonDecode(response.body));
    list = jsonMovieResponse.results;
    
    return list;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load movies');
  }
  http.get('https://api.themoviedb.org/3/search/movie\?api_key\=b67cf5e5937d794800a48aa688d0be9f\&language\=en-US\&query\=$pattern\&page\=1\&include_adult\=false');

  return list;
}
